import { createDiv, createFormInputGroup } from './helper.js';

export const modalForm = createDiv('modal fade modal-form');
modalForm.setAttribute('tabindex', '-1');
modalForm.setAttribute('role', 'dialog');
modalForm.setAttribute('aria-labelledby', 'mySsmallModalLabel');
modalForm.setAttribute('aria-hidden', 'true');

const modalDialog = modalForm.appendChild(createDiv('modal-dialog modal-lg'));
const modalContent = modalDialog.appendChild(createDiv('modal-content'));
const modalHeader = modalContent.appendChild(createDiv('modal-header'));
const titulo = document.createElement('h5');
titulo.className = 'modal-title';
titulo.textContent = 'Descripción';
const boton = document.createElement('button');
boton.type = 'button';
boton.className = 'close';
boton.setAttribute('data-dismiss', 'modal');
boton.setAttribute('aria-label', 'Close');
const spanBoton = document.createElement('span');
spanBoton.setAttribute('aria-hidden', 'true');
spanBoton.textContent = 'x';
boton.appendChild(spanBoton);

modalHeader.appendChild(titulo);
modalHeader.appendChild(boton);

const modalBody = modalContent.appendChild(createDiv('modal-body'));
const formulario = modalBody.appendChild(document.createElement('form'));

formulario.id = 'formularioAdopcion';
formulario.appendChild(createFormInputGroup('email', 'Correo electrónico', 'email', 'email', 'email', 'example@duoc.cl', true, ''));
formulario.appendChild(createFormInputGroup('Run', 'Run', 'text', 'run', 'Run', 'Run', true, '12'));
formulario.appendChild(createFormInputGroup('Nombre', 'Nombre Completo', 'text', 'nombre', 'Nombre', 'Nombre Apellido', true, ''));
formulario.appendChild(createFormInputGroup('fecha_nacimiento', 'Fecha de Nacimiento', 'text', 'fecha_nacimiento', 'fecha_nacimiento', '01/01/1990', true, ''));
formulario.appendChild(createFormInputGroup('Telefono', 'Teléfono de contacto', 'number', 'telefono', 'Telefono', '123456789', false, ''));

const inputRegion = createDiv('form-group');
const labelRegion = document.createElement('label');
labelRegion.setAttribute('for', 'region');
labelRegion.textContent = 'Región';
const selectRegion = document.createElement('select');
selectRegion.required = true;
selectRegion.name = 'region';
selectRegion.id = 'region';
selectRegion.className = 'form-control';
selectRegion.options[selectRegion.options.length] = new Option('Seleccione --', '-1');

inputRegion.appendChild(labelRegion);
inputRegion.appendChild(selectRegion);

formulario.appendChild(inputRegion);

const inputCity = createDiv('form-group');
const labelCity = document.createElement('label');
labelCity.setAttribute('for', 'ciudad');
labelCity.textContent = 'Ciudad';
const selectCity = document.createElement('select');
selectCity.disabled = true;
selectCity.required = true;
selectCity.name = 'ciudad';
selectCity.id = 'ciudad';
selectCity.className = 'form-control';
selectCity.options[selectCity.options.length] = new Option('Seleccione --', '-1');

inputCity.appendChild(labelCity);
inputCity.appendChild(selectCity);

formulario.appendChild(inputCity);

const inputVivienda = createDiv('form-group');
const labelVivienda = document.createElement('label');
labelVivienda.setAttribute('for', 'tipo_vivienda');
labelVivienda.textContent = 'Tipo de Vivienda';
const selectVivienda = document.createElement('select');
selectVivienda.disabled = true;
selectVivienda.required = true;
selectVivienda.name = 'tipo_vivienda';
selectVivienda.id = 'tipo_vivienda';
selectVivienda.className = 'form-control';
selectVivienda.options[selectVivienda.options.length] = new Option('Seleccione --', '-1');

inputVivienda.appendChild(labelVivienda);
inputVivienda.appendChild(selectVivienda);

formulario.appendChild(inputVivienda);

const submit = document.createElement('button');
submit.type = 'submit';
submit.className = 'btn btn-primary';
submit.textContent = 'Enviat';

formulario.appendChild(submit);