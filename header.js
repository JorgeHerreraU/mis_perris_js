import { createDiv, createImg, createNav, createLink, createCarousel, createFa } from './helper.js';

export const header = document.createElement('header');

const container = createDiv('container-fluid');

// First Row
const rowFirst = container.appendChild(createDiv('row first'));
const colDiv1 = rowFirst.appendChild(createDiv('col-md-4 col-12'));
const colDiv2 = rowFirst.appendChild(createDiv('col-md-4 col-12'));
colDiv1.appendChild(createImg('img/logo.png', '', 'width: 100%;', 'mis perris'));
const nav = colDiv2.appendChild(createNav('row', 'text-align: center;'));
nav.appendChild(createLink('col-md-3 col-12', '#', 'inicio'));
nav.appendChild(createLink('col-md-3 col-12', '#', 'quieres somos'));
nav.appendChild(createLink('col-md-3 col-12', '#', 'servicio'));
nav.appendChild(createLink('col-md-3 col-12', '#', 'contáctenos'));
rowFirst.appendChild(createDiv('col-md-4 col-12'));

// Second Row
const rowSecond = container.appendChild(createDiv('row second'));
rowSecond.appendChild(createCarousel('carouselAdoptados',
    ['img/adoptados/Apolo.jpg', 'img/adoptados/Duque.jpg', 'img/adoptados/Tom.jpg'], [
        {
            titulo: 'Apolo',
            parrafo: 'Agradecidos de adoptar a Apolo'
        },
        {
            titulo: 'Duque',
            parrafo: 'Duque llegó a alegrar nuestras vidas'
        },
        {
            titulo: 'Tom',
            parrafo: 'Tom ha sido lo mejor que nos ha pasado'
        }
    ]));

// Third Row
const rowThird = container.appendChild(createDiv('row third'));
const colDiv3 = rowThird.appendChild(createDiv('col-md-4 d-none d-md-block'));
colDiv3.innerHTML = '+56 9 55557777';
const colDiv4 = rowThird.appendChild(createDiv('col-md-4 d-none d-md-block'));
colDiv4.innerHTML = 'Rescate y adopción de perro callejeros';
const colDiv5 = rowThird.appendChild(createDiv('col-md-4'));
const rowSocial = colDiv5.appendChild(createDiv('row social'));

const colDiv6 = rowSocial.appendChild(createDiv('col-3'));
const circle1 = colDiv6.appendChild(createDiv('circle'));
const facebook = createFa('fab fa-facebook-f');
circle1.appendChild(facebook);

const colDiv7 = rowSocial.appendChild(createDiv('col-3'));
const circle2 = colDiv7.appendChild(createDiv('circle'));
const googlePlus = createFa('fab fa-google-plus-g');
circle2.appendChild(googlePlus);

const colDiv8 = rowSocial.appendChild(createDiv('col-3'));
const circle3 = colDiv8.appendChild(createDiv('circle'));
const instagram = createFa('fab fa-instagram');
circle3.appendChild(instagram);

const colDiv9 = rowSocial.appendChild(createDiv('col-3'));
const circle4 = colDiv9.appendChild(createDiv('circle'));
const envelop = createFa('fas fa-envelope');
circle4.appendChild(envelop);





header.prepend(container)