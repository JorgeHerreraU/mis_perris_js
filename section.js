import { createDiv, createImg, createNav, createLink, createCarousel, createFa } from './helper.js';

export const section = document.createElement('section');
section.className = 'gallery';

const container = createDiv('container');
const cardColumns = container.appendChild(createDiv('card-columns'));

const card1 = cardColumns.appendChild(createDiv('card'));
const image1 = document.createElement('img');
image1.className = 'card-img';
image1.src = 'img/rescatados/Oso.jpg';
image1.alt = 'Oso';
image1.setAttribute('data-info', 'Bonita');
image1.setAttribute('data-toggle', 'modal');
image1.setAttribute('data-target', '.modal-image');
card1.appendChild(image1);

const card2 = cardColumns.appendChild(createDiv('card'));
const image2 = document.createElement('img');
image2.className = 'card-img';
image2.src = 'img/rescatados/Maya.jpg';
image2.alt = 'Maya';
image2.setAttribute('data-info', 'Bonita');
image2.setAttribute('data-toggle', 'modal');
image2.setAttribute('data-target', '.modal-image');
card2.appendChild(image2);

const card3 = cardColumns.appendChild(createDiv('card'));
const image3 = document.createElement('img');
image3.className = 'card-img';
image3.src = 'img/rescatados/Wifi.jpg';
image3.alt = 'Wifi';
image3.setAttribute('data-info', 'Bonita');
image3.setAttribute('data-toggle', 'modal');
image3.setAttribute('data-target', '.modal-image');
card3.appendChild(image3);

const card4 = cardColumns.appendChild(createDiv('card'));
const image4 = document.createElement('img');
image4.className = 'card-img';
image4.src = 'img/rescatados/Chocolate.jpg';
image4.alt = 'Chocolate';
image4.setAttribute('data-info', 'Bonita');
image4.setAttribute('data-toggle', 'modal');
image4.setAttribute('data-target', '.modal-image');
card4.appendChild(image4);

const card5 = cardColumns.appendChild(createDiv('card'));
const image5 = document.createElement('img');
image5.className = 'card-img';
image5.src = 'img/rescatados/Pexel.jpg';
image5.alt = 'Pexel';
image5.setAttribute('data-info', 'Bonita');
image5.setAttribute('data-toggle', 'modal');
image5.setAttribute('data-target', '.modal-image');
card5.appendChild(image5);

const card6 = cardColumns.appendChild(createDiv('card'));
const image6 = document.createElement('img');
image6.className = 'card-img';
image6.src = 'img/rescatados/Bigotes.jpg';
image6.alt = 'Bigotes';
image6.setAttribute('data-info', 'Bonita');
image6.setAttribute('data-toggle', 'modal');
image6.setAttribute('data-target', '.modal-image');
card6.appendChild(image6);

const card7 = cardColumns.appendChild(createDiv('card'));
const image7 = document.createElement('img');
image7.className = 'card-img';
image7.src = 'img/rescatados/Luna.jpg';
image7.alt = 'Luna';
image7.setAttribute('data-info', 'Bonita');
image7.setAttribute('data-toggle', 'modal');
image7.setAttribute('data-target', '.modal-image');
card7.appendChild(image7);

section.prepend(container);
