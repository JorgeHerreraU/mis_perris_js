export const createDiv = classNames => {
    const div = document.createElement('div');
    div.className = classNames;
    return div;
}

export const createImg = (source, classNames, styles, alternative) => {
    const img = document.createElement('img');
    img.src = source;
    img.className = classNames;
    img.style = styles;
    img.alt = alternative;
    return img;
}

export const createNav = (classNames, styles) => {
    const nav = document.createElement('nav');
    nav.className = classNames;
    nav.style = styles;
    return nav;
}

export const createLink = (classNames, address, content) => {
    const link = document.createElement('a');
    link.className = classNames;
    link.href = address;
    link.innerHTML = content;
    return link;
}

export const createCarousel = (identifier, pics, info) => {
    const div = document.createElement('div');
    div.setAttribute('id', identifier);
    div.className = 'carousel slide w-100',
        div.setAttribute('data-ride', 'carousel');

    const ol = div.appendChild(document.createElement('ol'));
    ol.className = 'carousel-indicators';

    const picsLength = pics.length;

    for (let i = 0; i < picsLength; i++) {
        const li = document.createElement('li');
        if (i === 0) {
            li.className = 'active';
        }

        li.setAttribute('data-target', `#${identifier}`);
        li.setAttribute('data-slide-to', i);
        ol.appendChild(li);
    }

    const inner = div.appendChild(createDiv('carousel-inner'));

    for (let i = 0; i < picsLength; i++) {
        const carouselItem = createDiv('carousel-item');
        if (i === 0) {
            carouselItem.classList.add('active');
        }
        carouselItem.appendChild(createImg(`${pics[i]}`, 'd-block w-100', '', `${i} Slide`));
        const carouselCaption = carouselItem.appendChild(createDiv('carousel-caption d-none d-md-block'));
        carouselCaption.innerHTML = `<h5>${info[i].titulo}</h5> <p>${info[i].parrafo}</p>`;
        inner.appendChild(carouselItem);
    }
    const spanPrev = document.createElement('span');
    spanPrev.className = 'carousel-control-prev-icon';
    spanPrev.setAttribute('aria-hidden', 'true');

    const spanNext = document.createElement('span');
    spanNext.className = 'carousel-control-next-icon';
    spanNext.setAttribute('aria-hidden', 'true');

    const srPrev = document.createElement('span');
    srPrev.className = 'sr-only';
    srPrev.textContent = 'Previous';

    const srNext = document.createElement('span');
    srNext.className = 'sr-only';
    srNext.textContent = 'Next';

    const prev = createLink('carousel-control-prev', '#carouselAdoptados', '', '');
    prev.setAttribute('role', 'button');
    prev.setAttribute('data-slide', 'prev');
    prev.appendChild(spanPrev);
    prev.appendChild(srPrev);

    const next = createLink('carousel-control-next', '#carouselAdoptados', '', '');
    next.setAttribute('role', 'button');
    next.setAttribute('data-slide', 'next');
    next.appendChild(spanNext);
    next.appendChild(srNext);

    div.appendChild(prev);
    div.appendChild(next);

    return div;
}

export const createFa = iconName => {
    const icon = document.createElement('i');
    icon.className = iconName;
    return icon;
}

export const createFormInputGroup = (labelFor, labelText, inputType, inputName, inputId, inputPlaceHolder, isRequired, inputMaxLength) => {
    const div = createDiv('form-group');
    const label = div.appendChild(document.createElement('label'));
    label.setAttribute('for', labelFor);
    label.textContent = labelText;
    const input = div.appendChild(document.createElement('input'));
    input.type = inputType;
    input.className = 'form-control';
    input.name = inputName;
    input.id = inputId;
    input.placeholder = inputPlaceHolder;
    if (isRequired) {
        input.required = true;
    }
    if (inputMaxLength != '') {
        input.maxLength = inputMaxLength;
    }
    return div;
}