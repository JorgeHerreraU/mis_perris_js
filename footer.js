import { createDiv } from './helper.js';

export const footer = document.createElement('footer');

const container = footer.appendChild(createDiv('container'));
const parrafo = container.appendChild(document.createElement('p'));
parrafo.textContent = 'Desarrollo Web y Mobile';