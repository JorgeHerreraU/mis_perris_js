import { createDiv, createImg, createNav, createLink, createCarousel, createFa } from './helper.js';

export const article = document.createElement('article');
const container = createDiv('container');
const row1 = container.appendChild(article.appendChild(createDiv('row')));
const col1 = row1.appendChild(createDiv('col-md-6 cl-12'));
const hr = '<hr>'
const titulo = '<h1>RESCATE</h1>';
const subtitulo = '<h3>ETAPA UNO</h3>';
const parrafo = '<p>Rescatamos perros en situación de peligro y/o abandono. Los rehabilitamos y preparamos para buscarles hogar </p>'
col1.innerHTML = titulo + subtitulo + hr + parrafo;
const img = createImg('img/rescate.jpg', 'w-100', '', 'rescate-img');
col1.appendChild(img);
const col2 = row1.appendChild(createDiv('col-md-6 cl-12'));
const title2 = '<h1>CROWFUNDING</h1>';
const subtitulo2 = '<h3>FINACIAMIENTO</h3>';
const parrafo2 = '<p>Sigue nuestras redes sociales para inforcarme acerta de las diversas campañas y actividades que realizaremos para obtener financiamiento para seguir ayudando</p>'
const br = '<br>';
col2.innerHTML = title2 + subtitulo2 + hr + parrafo2 + br;
col2.prepend(createImg('img/crowfunding.jpg', 'w-100', '', 'crow-funding'));
const boton = document.createElement('button');
boton.className = 'campaña';
boton.setAttribute('data-toggle', 'modal');
boton.setAttribute('data-target', '.modal-form');
boton.textContent = 'CAMPAÑAS';
col2.appendChild(boton);

article.prepend(container);