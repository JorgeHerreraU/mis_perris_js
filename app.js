import { header } from './header.js';
import { article } from './article.js';
import { section } from './section.js';
import { footer } from './footer.js';
import { modal } from './modal.js';
import { modalForm } from './modal-form.js';


const body = document.querySelector('#body');

body.prepend(header)
header.after(article);
article.after(section);
section.after(footer);
footer.after(modal);
modal.after(modalForm);